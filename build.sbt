import scala.xml.NodeSeq

val commonSettings = Seq(
  scalaVersion := "2.11.7",
  scalacOptions ++= Seq("-Xfatal-warnings", "-feature", "-deprecation"))

lazy val extradoc = project.in(file(".")).
  settings(commonSettings: _*).
  settings(
    libraryDependencies ++= Seq(
      "org.scala-lang" % "scala-compiler" % "2.11.7"
    )
  )

