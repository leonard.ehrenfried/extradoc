package com.novocode.extradoc

import scala.tools.nsc._

class ExtraDocSettings(error: String => Unit) extends doc.Settings(error) {

  override def ChoiceSetting(name: String, helpArgs: String, descr: String, choices: List[String], default: String) = {
    if(name == "-doc-format") super.ChoiceSetting(name, helpArgs, descr, List("html", "json", "json-multi", "explorer"), default)
    else super.ChoiceSetting(name, descr, helpArgs, choices, default)
  }
}
